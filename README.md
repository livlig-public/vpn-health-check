# VPN Health Check Script

Checks if your VPN is running, and if not attempts to restart the VPN. Intended to be run on a schedule with cron and optional alerting using Healthchecks.io.  

Works by comparing VPN public IP with local public IP using https://icanhazip.com.  
If IPs are the same, indicating something is wrong with the VPN:  
-  a service is stopped (default qBittorrent)
-  vpn is restarted (default Wireguard)
-  wait progressively longer before trying again
-  if the IPs still match after 11 attempts, a reboot is issued.
-  will progressively backoff longer after each reboot

Failures are logged to /var/log/vpn_healthcheck.log. Successes are logged after failures, and are recorded by healthchecks.io.

# Requirements
- Healthchecks.io (optional) or anopther ping based monitor, configured for youre prefered schedule (eg once per minute). When the script fails to check in, Healthchecks.io will send an alert.
- A systemd service to stop (default qBittorrent)
- A systemd VPN service to restart (default Wireguard)

# Installation
Download the [latest release](https://gitlab.com/livlig-public/vpn-health-check/-/releases) and make it executable: `chmod 755 /vpn_healthcheck.sh`  

# Configuration
Configure the cronjob:  
```
sudo crontab -e
* * * * /root/vpn_healthcheck.sh -u  https://hc-ping.com/your-UUID
```    

# Notes
- run `vpn_healthcheck.sh -h` for help and usage.
- Automatically selects the **FIRST** en\* or eth\* interface (eg eth0 if both eth0 and eth1 exist). Can be manually set with -n.
- If the VPN interface cannont curl icanhazip.com then the check will fail
- If the VPN interface can curl it's IP and non-vpn interface cannot (ie blocked by a firewall) then the check will pass (wont match VPN IP).
- Use `./vpn_healthcheck.sh -d` to run in debug mode.
- Stopping qbittorrent is an extra precaution, you should bind your torrent client to your VPN interface.
- Config file default is `~/.vpn_healthcheck`. Currently only tracks number of reboots.

# Known issues
- Only short flags work, ie `-d` works while `--debug` does not.

# To Do
- test reboot if statement code
- update downloader
- longer args
- option to store variables in config file
