#!/bin/bash
ver=1.1.0
function help(){
    echo "vpn_healthcheck version $ver
Website: https://gitlab.com/livlig-public/vpn-health-check
    
vpn_healthcheck comes with ABSOLUTELY NO WARRANTY.  This is free software, and
you are welcome to redistribute it under certain conditions.  See the GNU
General Public Licence for details
    
vpn_healthcheck checks if a VPN is working, and if not, attempts to restart the
VPN. Intended to be run on a schedule with cron and includes optional alerting
using pings, such as Healthchecks.io. Tracks repeated reboots using a file.
    
USAGE: vpn_healthcheck.sh [OPTIONS]
  or   vpn_healthcheck.sh -u https://hc-ping.com/UUID
  or   vpn_healthcheck.sh -s qbittorrent
  or   vpn_healthcheck.sh -v wg-quick@wg0
  or   vpn_healthcheck.sh -c ~/.vpn_healthcheck
  or   vpn_healthcheck.sh -dfbr
  or   vpn_healthcheck.sh -h
   
Options
  -u, --url                   set url to ping (e.g. healthchecks.io)
  -i, --url2                  set a second url to ping (e.g. uptimekuma)
  -s, --service               specify a service to restart (default: qbittorrent)
  -p, --vpn                   specify vpn service (default: wireguard: 'wg-quick@wg0')
  -n, --nic                   specify network interface (default: en* or eth* e.g. enp0s25 or eth0)
  -c, --config                specify location of config file (default: ~/.vpn_healthcheck)
  -d, --debug                 debug mode, verbose and log successes
  -f, --fail                  simulates a failure (stops VPN before checking)
  -b, --nobackoff             skips backoff (no delay betwen VPN restart attempts)
  -r, --noreboot              won't reboot after excessive failure
  -h, --help                  show this help"
}

conf=~/.vpn_healthcheck
log=/var/log/vpn_healthcheck.log
serv=qbittorrent
vpn=wg-quick@wg0
nic=$(ip -br l | cut -d\  -f1 | grep -m 1 -E "en|eth") #selects interface (e.g. enp0s25 or eth0)

while getopts ":u:i:s:p:n:c:dfbruh" ARG; do
  case "$ARG" in
    u) pingurl=${OPTARG} ;;
    i) pingurl2=${OPTARG} ;;
    s) serv=${OPTARG} ;;
    p) vpn=${OPTARG} ;;
    n) nic=${OPTARG} ;;
    c) conf=${OPTARG} ;;
    d) set -x; set -v; debug=1 ;;
    f) systemctl stop $vpn ;;
    b) skipbackoff=1 ;;
    r) skipreboot=1 ;;
    h) help ;;
  esac
done

if ! test -f $conf || ! grep -qx 'reboots [0-9]*' $conf; then
  echo "reboots 0" > $conf #if .vpn_healthcheck doesnt exist, or is mangled, initialize it
fi

another_instance()
{
  echo `date +"%Y-%m-%d %T"` Another instance is running, exiting. >> $log
  exit 1
}
( flock -n 100 || another_instance #flock ensures only one instance is running
  i=0
  until [ $(curl -s icanhazip.com) != $(curl -s --interface $nic icanhazip.com || echo failed) ] #loop until if VPN public IP is not equal to local public ip
  do
    if [[ $i = 0 ]]; then #runs reboot backoff on first loop
      reboots=$(cat $conf | grep reboots | awk '{print $2;}')
      backoff=$(($reboots*$reboots*60))
      sleep $backoff
    fi
    if [[ -z "$skipreboot" ]] && [[ $i -gt 11 ]]; then # i=11 will loop ~= 10min
      echo `date +"%Y-%m-%d %T"` "Too many failures, rebooting" >> $log
      $reboots=$(( reboots + 1 ))
      sed -i 's/reboots [0-9]*/reboots '$reboots'/g' $conf #update reboot counter
      reboot now
      exit 1 #quit without pinging healthchecks.io, which triggers an alert
    fi
    echo `date +"%Y-%m-%d %T"` VPN IP = Public IP! Stopping $serv. Restarting $vpn >> $log
    timeout 30s systemctl stop $serv #end command and error if stopping takes longer than 30s
    [ $? != 0 ] && systemctl kill $serv #if previous command did not exit successfully, kills $serv
    systemctl restart $vpn
    sleeptime=$(($i*10)) #wait exponentially longer each time before retrying
    [ -z "$skipbackoff" ] && sleep $sleeptime
    ((i+=1))
  done
  if [ "$i" != 0 ] || [ "$debug" = '1' ]; then #only if there was a problem, or debug mode
    echo `date +"%Y-%m-%d %T"` OK. IPs are different. >> $log
    systemctl start $serv
    sed -i 's/reboots [0-9]*/reboots 0/g' $conf #resets reboot counter
  fi
  [ ! -z "$pingurl" ] && curl -fsS -m 10 --retry 5 -o /dev/null $pingurl #ping (eg heathchecks.io, uptimekuma) to repot OK, if $pingurl is not empty
  [ ! -z "$pingurl2" ] && curl -fsS -m 10 --retry 5 -o /dev/null $pingurl2 #ping another URL
) 100>/var/lock/vpn_healthcheck.lock
